package epam.university.yunusov.model.entity;

public class Display {

  private float size;
  private String resolution;
  private String matrixType;

  public float getSize() {
    return size;
  }

  public void setSize(float size) {
    this.size = size;
  }

  public String getResolution() {
    return resolution;
  }

  public void setResolution(String resolution) {
    this.resolution = resolution;
  }

  public String getMatrixType() {
    return matrixType;
  }

  public void setMatrixType(String matrixType) {
    this.matrixType = matrixType;
  }

  @Override
  public String toString() {
    return "Display{" +
        "size=" + size +
        ", resolution='" + resolution + '\'' +
        ", matrixType='" + matrixType + '\'' +
        '}';
  }
}
