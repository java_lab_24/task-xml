package epam.university.yunusov.model.entity;

public class Hardware {

  private String cpu;
  private String gpu;
  private int ram;

  public String getCpu() {
    return cpu;
  }

  public void setCpu(String cpu) {
    this.cpu = cpu;
  }

  public String getGpu() {
    return gpu;
  }

  public void setGpu(String gpu) {
    this.gpu = gpu;
  }

  public int getRam() {
    return ram;
  }

  public void setRam(int ram) {
    this.ram = ram;
  }

  @Override
  public String toString() {
    return "Hardware{" +
        "cpu='" + cpu + '\'' +
        ", gpu='" + gpu + '\'' +
        ", ram=" + ram +
        '}';
  }
}
