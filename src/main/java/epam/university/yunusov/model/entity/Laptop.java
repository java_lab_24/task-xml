package epam.university.yunusov.model.entity;

public class Laptop {

  private int laptopId;
  private String name;
  private String origin;
  private int price;
  private Hardware hardware;
  private Display display;

  public int getLaptopId() {
    return laptopId;
  }

  public void setLaptopId(int laptopId) {
    this.laptopId = laptopId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOrigin() {
    return origin;
  }

  public void setOrigin(String origin) {
    this.origin = origin;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Hardware getHardware() {
    return hardware;
  }

  public void setHardware(Hardware hardware) {
    this.hardware = hardware;
  }

  public Display getDisplay() {
    return display;
  }

  public void setDisplay(Display display) {
    this.display = display;
  }

  @Override
  public String toString() {
    return "Laptop{" +
        "laptopId=" + laptopId +
        ", name='" + name + '\'' +
        ", origin='" + origin + '\'' +
        ", price=" + price +
        ", hardware=" + hardware +
        ", display=" + display +
        '}';
  }
}
