package epam.university.yunusov.model.parser.sax;

import epam.university.yunusov.model.entity.Laptop;
import java.io.File;
import java.util.List;

public class Main {

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\xml\\laptops.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\laptops.xsd");

    List<Laptop> devices = SaxParser.parse(xmlFile, xsdFile);
    System.out.println(devices);
  }

}
