package epam.university.yunusov.model.parser.sax;

import epam.university.yunusov.model.entity.Laptop;
import epam.university.yunusov.model.xml_validator.XmlValidator;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.SAXException;

public class SaxParser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Laptop> parse(File xml, File xsd) {
    List<Laptop> laptops = new ArrayList<>();
    try {
      saxParserFactory.setSchema(XmlValidator.createSchema(xsd));
      saxParserFactory.setValidating(true);

      SAXParser saxParser = saxParserFactory.newSAXParser();
      saxParser.isValidating();
      SaxHandler saxHandler = new SaxHandler();
      saxParser.parse(xml, saxHandler);
      laptops = saxHandler.getLaptopList();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }
    return laptops;
  }
}