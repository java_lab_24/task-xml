package epam.university.yunusov.model.parser.dom;

import epam.university.yunusov.model.entity.Display;
import epam.university.yunusov.model.entity.Hardware;
import epam.university.yunusov.model.entity.Laptop;
import java.util.ArrayList;
import java.util.List;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMParser {

  public static List<Laptop> parse(Document document) {
    List<Laptop> devices = new ArrayList<>();
    NodeList nodeList = document.getElementsByTagName("laptop");
    for (int index = 0; index < nodeList.getLength(); index++) {
      Laptop laptop = new Laptop();
      Node node = nodeList.item(index);
      Element element = (Element) node;
      laptop.setLaptopId(Integer.parseInt(element.getAttribute("laptopId")));
      laptop.setName(element.getElementsByTagName("name").item(0).getTextContent());
      laptop.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());
      laptop.setPrice(Integer.parseInt(element.getElementsByTagName("price").item(0)
          .getTextContent()));
      laptop.setHardware(getHardware(element.getElementsByTagName("hardware")));
      laptop.setDisplay(getDisplay(element.getElementsByTagName("display")));
      devices.add(laptop);
    }
    return devices;
  }

  private static Hardware getHardware(NodeList nodeList) {
    Hardware hardware = new Hardware();
    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      hardware.setCpu(element.getElementsByTagName("cpu").item(0).getTextContent());
      hardware.setGpu(element.getElementsByTagName("gpu").item(0).getTextContent());
      hardware.setRam(Integer.parseInt(element.getElementsByTagName("ram").item(0).getTextContent()));
    }
    return hardware;
  }

  private static Display getDisplay(NodeList nodeList) {
    Display display = new Display();
    if (nodeList.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodeList.item(0);
      display.setSize(Float.parseFloat(element.getElementsByTagName("size").item(0).getTextContent()));
      display.setResolution(element.getElementsByTagName("resolution").item(0).getTextContent());
      display.setMatrixType(element.getElementsByTagName("matrixType").item(0).getTextContent());
    }
    return display;
  }
}
