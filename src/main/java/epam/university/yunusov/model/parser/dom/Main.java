package epam.university.yunusov.model.parser.dom;

import epam.university.yunusov.model.entity.Laptop;
import epam.university.yunusov.model.xml_validator.XmlValidator;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class Main {

  private static DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
  private static DocumentBuilder documentBuilder;

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\xml\\laptops.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\laptops.xsd");

    Document document = null;
    try {
      documentBuilder = factory.newDocumentBuilder();
      document = documentBuilder.parse(xmlFile);
    } catch (ParserConfigurationException | SAXException | IOException e) {
      e.printStackTrace();
    }

    if (XmlValidator.validate(document, xsdFile)) {
      List<Laptop> deviceList = DOMParser.parse(document);
      System.out.println(deviceList);
    } else {
      System.out.println("XML document failed validation.");
    }
  }

}
