package epam.university.yunusov.model.parser.sax;

import epam.university.yunusov.model.entity.Display;
import epam.university.yunusov.model.entity.Hardware;
import epam.university.yunusov.model.entity.Laptop;
import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SaxHandler extends DefaultHandler {

  private final String LAPTOP_TAG = "laptop";
  private final String NAME_TAG = "name";
  private final String ORIGIN_TAG = "origin";
  private final String PRICE_TAG = "price";
  private final String SPECIFICATION_TAG = "specification";
  private final String HARDWARE_TAG = "hardware";
  private final String CPU_TAG = "cpu";
  private final String GPU_TAG = "gpu";
  private final String RAM_TAG = "ram";
  private final String DISPLAY_TAG = "display";
  private final String SIZE_TAG = "size";
  private final String RESOLUTION_TAG = "resolution";
  private final String MATRIX_TYPE_TAG = "matrixType";

  private final String LAPTOP_ID_ATTRIBUTE = "laptopId";

  private List<Laptop> laptops = new ArrayList<>();
  private Laptop laptop;
  private Hardware hardware;
  private Display display;
  private String currentElement;

  public List<Laptop> getLaptopList() {
    return this.laptops;
  }

  @Override
  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    currentElement = qName;
    switch (currentElement) {
      case LAPTOP_TAG : {
        String laptopId = attributes.getValue(LAPTOP_ID_ATTRIBUTE);
        laptop = new Laptop();
        laptop.setLaptopId(Integer.parseInt(laptopId));
        break;
      }
      case HARDWARE_TAG : {
        hardware = new Hardware();
        break;
      }
      case DISPLAY_TAG : {
        display = new Display();
        break;
      }
    }
  }

  @Override
  public void endElement(String uri, String localName, String qName) throws SAXException {
    switch (qName) {
      case LAPTOP_TAG : {
        laptops.add(laptop);
        break;
      }
      case HARDWARE_TAG : {
        laptop.setHardware(hardware);
        hardware = null;
        break;
      }
      case DISPLAY_TAG : {
        laptop.setDisplay(display);
        display = null;
        break;
      }
    }
  }

  @Override
  public void characters(char[] ch, int start, int length) throws SAXException {
    if (currentElement.equals(NAME_TAG)) {
      laptop.setName(new String(ch, start, length));
    }
    if (currentElement.equals(ORIGIN_TAG)) {
      laptop.setOrigin(new String(ch, start, length));
    }
    if (currentElement.equals(PRICE_TAG)) {
      laptop.setPrice(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(CPU_TAG)) {
      hardware.setCpu(new String(ch, start, length));
    }
    if (currentElement.equals(GPU_TAG)) {
      hardware.setGpu(new String(ch, start, length));
    }
    if (currentElement.equals(RAM_TAG)) {
      hardware.setRam(Integer.parseInt(new String(ch, start, length)));
    }
    if (currentElement.equals(SIZE_TAG)) {
      display.setSize(Float.parseFloat(new String(ch, start, length)));
    }
    if (currentElement.equals(RESOLUTION_TAG)) {
      display.setResolution(new String(ch, start, length));
    }
    if (currentElement.equals(MATRIX_TYPE_TAG)) {
      display.setMatrixType(new String(ch, start, length));
    }
  }
}
