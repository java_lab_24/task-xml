package epam.university.yunusov.model.parser.stax;

import java.io.File;

public class Main {

  public static void main(String... args) {
    File xmlFile = new File("src\\main\\resources\\xml\\laptops.xml");
    File xsdFile = new File("src\\main\\resources\\xml\\laptops.xsd");
    StaxParser staxParser = new StaxParser();
    System.out.println(staxParser.parse(xmlFile));
  }

}
