package epam.university.yunusov.model.parser.stax;

import epam.university.yunusov.model.entity.Display;
import epam.university.yunusov.model.entity.Hardware;
import epam.university.yunusov.model.entity.Laptop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class StaxParser {

  private final String LAPTOP_TAG = "laptop";
  private final String NAME_TAG = "name";
  private final String ORIGIN_TAG = "origin";
  private final String PRICE_TAG = "price";
  private final String SPECIFICATION_TAG = "specification";
  private final String HARDWARE_TAG = "hardware";
  private final String CPU_TAG = "cpu";
  private final String GPU_TAG = "gpu";
  private final String RAM_TAG = "ram";
  private final String DISPLAY_TAG = "display";
  private final String SIZE_TAG = "size";
  private final String RESOLUTION_TAG = "resolution";
  private final String MATRIX_TYPE_TAG = "matrixType";

  private final String LAPTOP_ID_ATTRIBUTE = "laptopId";

  private XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();

  private List<Laptop> laptops = new ArrayList<>();
  private Laptop laptop;
  private Hardware hardware;
  private Display display;
  private String currentElement;

  public List<Laptop> parse(File xml) {
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case LAPTOP_TAG: {
              laptop = new Laptop();
              Attribute laptopId = startElement.getAttributeByName(new QName(LAPTOP_ID_ATTRIBUTE));
              if (laptopId != null) {
                laptop.setLaptopId(Integer.parseInt(laptopId.getValue()));
              }
              break;
            }
            case NAME_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              laptop.setName(xmlEvent.asCharacters().getData());
              break;
            }
            case ORIGIN_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              laptop.setOrigin(xmlEvent.asCharacters().getData());
              break;
            }
            case PRICE_TAG: {
              xmlEvent = xmlEventReader.nextEvent();
              laptop.setPrice(Integer.parseInt(xmlEvent.asCharacters().getData()));
              break;
            }
            case HARDWARE_TAG: {
              hardware = new Hardware();
              break;
            }
            case CPU_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              hardware.setCpu(xmlEvent.asCharacters().getData());
            }
            case GPU_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              hardware.setGpu(xmlEvent.asCharacters().getData());
            }
            case RAM_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              hardware.setRam(Integer.parseInt(xmlEvent.asCharacters().getData()));
            }
            case DISPLAY_TAG: {
              display = new Display();
              break;
            }
            case SIZE_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              display.setSize(Float.parseFloat(xmlEvent.asCharacters().getData()));
            }
            case RESOLUTION_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              display.setResolution(xmlEvent.asCharacters().getData());
            }
            case MATRIX_TYPE_TAG : {
              xmlEvent = xmlEventReader.nextEvent();
              display.setMatrixType(xmlEvent.asCharacters().getData());
            }
          }
        }
        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          String name = endElement.getName().getLocalPart();
          switch (name) {
            case LAPTOP_TAG : {
              laptops.add(laptop);
              laptop = null;
              break;
            }
            case HARDWARE_TAG : {
              laptop.setHardware(hardware);
              hardware = null;
              break;
            }
            case DISPLAY_TAG : {
              laptop.setDisplay(display);
              display = null;
              break;
            }
          }
        }
      }
    } catch (XMLStreamException | FileNotFoundException e) {
      e.printStackTrace();
    }
    return laptops;
  }
}

