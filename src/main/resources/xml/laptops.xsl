<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <html>
      <body style="font-family: Arial; font-size: 12pt; background-color: #EEE">
        <div style="background-color: green; color: black;">
          <h2>L A P T O P S</h2>
        </div>
        <table border="3">
          <tr bgcolor="#2E9AFE">
            <th>ID</th>
            <th>Name</th>
            <th>Country</th>
            <th>Price</th>
            <th>Hardware</th>
            <th>Display</th>
          </tr>

          <xsl:for-each select="laptops/laptop">
            <tr>
              <td><xsl:value-of select="@laptopId"/></td>
              <td><xsl:value-of select="name"/></td>
              <td><xsl:value-of select="origin"/></td>
              <td>
                <xsl:value-of select="price"/>
                <xsl:text> UAH</xsl:text>
              </td>
              <td>
                <xsl:for-each select="specifications/hardware">
                  <xsl:value-of select="cpu"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="gpu"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="ram"/>
                  <xsl:text> gb </xsl:text>
                </xsl:for-each>
              </td>
              <td>
                <xsl:for-each select="specifications/display">
                  <xsl:value-of select="size"/>
                  <xsl:text> inch, </xsl:text>
                  <xsl:value-of select="resolution"/>
                  <xsl:text>, </xsl:text>
                  <xsl:value-of select="matrixType"/>
                </xsl:for-each>
              </td>
            </tr>
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>